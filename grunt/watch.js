/** 
 * modul 'grunt-contrib-watch' : sleduje zmeny a na zaklade toho spousti procesy
 * git https://github.com/gruntjs/grunt-contrib-watch
 */

'use strict';

module.exports = {
    options: {
      spawn: false,
    },

    /**
     * watch inlinecss 
     */
    default: {
        files: ['<%= path.cwd %>/**/*'],
        tasks: ['less', 'inlinecss']
    }

}; // module.exports