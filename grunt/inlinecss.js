/**
 * modul 'grunt-inline-css' : prida inline CSS
 * git https://github.com/jgallen23/grunt-inline-css
 */

'use strict';

module.exports = {
    /**
     * nastaveni pro vsechny tasky
     */
    options: {
        removeStyleTags: false,
        webResources: {
            images: false
        }
    },

    /**
     * defaultni kompilace inliner '<%= path.cwd %>/index.html' do '<%= path.dest %>/index.html'
     */
    default: {
        files: {
            '<%= path.dest %>/index.html' : '<%= path.cwd %>/index.html'  //vystupni adresar : zdrojovy soubor
        }
    }

}; // module.exports