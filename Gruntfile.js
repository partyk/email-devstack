'use strict';

module.exports = function (grunt) {
    var options = {
        name: "grunt-email-devstack",
        url: "",
        //cesty
        path: {
            cwd : 'cwd', //zdrojovy adresar/current working directory
            dest : 'dest', //vystup /destination
        },

    
        // External configs
        pkg: grunt.file.readJSON('package.json')
    };

    /**
	 * modul 'time-grunt' : pro debug. Zobrazi delku jednotlivych procesu
	 */
    require('time-grunt')(grunt);    
    require('load-grunt-config')(grunt, { config: options }); 
};