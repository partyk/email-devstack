# Email DevStack

Grunt inline CSS generator for email.

---

## How to start

1.  Download or clone repository
2.  run ```npm install```
3.  run ```grunt``` and develop or ```grunt build``` for production

## Resources

-   [NPM](https://www.npmjs.com/)
-   [Grunt](http://gruntjs.com)